//
//  CRISPR_ABmodel_P_DI.cpp
//
//  Created by Emma Keizer on 26/04/2021.
//  Copyright © 2021 Emma Keizer. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <array>
#include <math.h>
#include <random>
#include <algorithm>
#include "stocc.h"

using namespace std;
uniform_real_distribution<double> dis(0.0,1.0); // U[0,1) distribution
random_device r_u;
seed_seq seed_u{r_u(), r_u(), r_u(), r_u(), r_u(), r_u(), r_u(), r_u()}; // seed
mt19937 gen_u(seed_u); // generator
random_device r_n;
seed_seq seed_n{r_n(), r_n(), r_n(), r_n(), r_n(), r_n(), r_n(), r_n()}; // seed
mt19937 gen_n(seed_n); // generator
random_device r_b; // Bionomial distribution
seed_seq seed_b{r_b(), r_b(), r_b(), r_b(), r_b(), r_b(), r_b(), r_b()}; // seed
mt19937 gen_b(seed_b); // generator
random_device seed ;
mt19937 engine( seed( ) ) ;
// make random seed
int32_t seed2 = (int32_t)time(0);    // random seed = time
StochasticLib1 sto(seed2);
random_device r_l;
seed_seq seed_l{r_l(), r_l(), r_l(), r_l(), r_l(), r_l(), r_l(), r_l()}; // seed
mt19937 gen_l(seed_l); // generator

geometric_distribution<int> burst(1/double(3)); // Burst size distribution YFP (normal: 3)
geometric_distribution<int> burstCas(1/double(3)); // Burst size distribution Cascade (normal: 3, high: 201)
geometric_distribution<int> burstF(1/double(5)); // Burst size distribution Fragments
random_device r_g;
seed_seq seed_g{r_g(), r_g(), r_g(), r_g(), r_g(), r_g(), r_g(), r_g()}; // seed
mt19937 gen_g(seed_g); // generator for burst dist
/************************************************************
 *                                                          *
 *                      Classes                             *
 *                                                          *
 ************************************************************/
template<typename T>
class Counter {
public:
    Counter() { ++count; }
    Counter(const Counter&) { ++count; }
    ~Counter() { --count; }
    
    static int howMany()
    { return count; }
    
private:
    static int count;
};

template<typename T>
int//size_t
Counter<T>::count = 0;

class Cell{
    static int count;
    Counter<Cell> c;
public:
    double tBirth, tDiv;
    double VBirth;
    double tau;
    double mu_cell;
    double mu_cell_sum;
    double tgen_cell;
    double tgen_cell_sum;
    double tau_cell=0;
    int taugen_cell=0;
    int cleared=0;
    vector<int> history;
    array<int,13> x;
    static int howMany()
    {return Counter<Cell>::howMany();}
    int S[19][13]={
        //x0  x1  x2  x3  x4  x5  x6  x7  x8  x9 x10 x11
        {  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}, //a
        {  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}, //b
        {  0, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}, //c
        {  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0}, //d
        {  0,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0}, //e
        {  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0}, //f
        //{  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0}, //ff (DI)
        {  0,  0,  0,  0,  0,  1,  1,  0,  0,  0,  0,  0}, //ff (P)
        {  0,  0,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0}, //g
        {  0,  0,  0,  0,  0,  0, -1,  0,  0,  0,  0,  0}, //gg
        {  0,  0,  0,  0, -1, -1,  0,  1,  0,  0,  0,  0}, //h
        {  0,  0,  0,  0, -1,  0, -1,  0,  1,  0,  0,  0}, //hh
        { -1,  0,  0,  0,  0,  0,  0, -1,  0,  1,  0,  0}, //i
        {  1,  0,  0,  0,  0,  0,  0,  1,  0, -1,  0,  0}, //j
        { -1,  0,  0,  0,  0,  0,  0,  0, -1,  0,  1,  0}, //k
        {  1,  0,  0,  0,  0,  0,  0,  0,  1,  0, -1,  0}, //l
        {  0,  0,  0,  0,  0,  0,  0,  1,  0, -1,  0,  1}, //m
        {  0,  0,  0,  0,  0,  0,  0,  0,  1,  0, -1,  1}, //n
        {  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -1}, //o
        {  0,  0, -1,  1,  0,  0,  0,  0,  0,  0,  0, -1}};//p
    Cell(vector<int>& v, array<int, 13>& mol)
    : history(v), x(mol)
    { }
    ~Cell(){ }
    
    void print_hist(){
        for (int i = 0; i < history.size(); i++)
            cout << history[i] << " ";
        cout<<endl;
    }
    
    void update_x(int reaction) {
        int b;
        if (reaction==1||reaction==5||reaction==6){
            b=burst(gen_g);
            for (int i=0; i<x.size(); i++){x[i]+=b*S[reaction][i];
            }
        } else if (reaction==3){
            b=burstCas(gen_g);
            for (int i=0; i<x.size(); i++){x[i]+=b*S[reaction][i];
            }
        } else if (reaction==15||reaction==16){
            for (int i=0; i<x.size(); i++){
                if (i==11) {
                    b=burstF(gen_g);
                    x[i]+=b*S[reaction][i];
                } else {
                    x[i]+=S[reaction][i];
                }
            }
        } else {
            for (int i=0; i<x.size(); i++){x[i]+=S[reaction][i];
            }
        }
    }
    
    void print_x() {for (int i=0; i<x.size(); i++){
        cout << x[i] << " ";}
        cout<<endl;}
};

/************************************************************
 *                                                          *
 *                     Functions                            *
 *                                                          *
 ************************************************************/
void update_propensities(double a[], double f[], array<int,13> x, double VB, double Vt, double t, double mu, int pstar){
    if (mu>=f[0]){
        cout<<"mu>=f[0]"<<endl;
    }
    double delay=(1/(1+exp(-f[16]*t)));
    
    double p0=(double(pstar)/VB)/sqrt(f[0]/mu-1);
    int allP=x[0]+x[9]+x[10];

    a[0]=f[0]*pow(allP,1)/(1+pow((allP/Vt)/p0,2)); //a
    a[1]=f[1]*allP; //b
    a[2]=f[2]*x[1]; //c
    a[3]=delay*f[3]; //d
    a[4]=f[4]*x[4]; //e
    a[5]=f[5]*x[2]; //f
    a[6]=f[5]*x[3]; //ff
    a[7]=f[6]*x[5]; //g
    a[8]=f[6]*x[6]; //gg
    a[9]=(1/Vt)*f[7]*x[4]*x[5]; //h
    a[10]=(1/Vt)*f[7]*x[4]*x[6]; //hh
    a[11]=(1/Vt)*f[8]*x[0]*x[7]; //i
    a[12]=f[9]*x[9]; //j
    a[13]=(1/Vt)*f[10]*x[0]*x[8]; //k
    a[14]=f[11]*x[10]; //l
    a[15]=f[12]*x[9]; //m
    a[16]=f[13]*x[10]; //n
    a[17]=f[14]*x[11]; //o
    a[18]=(1/Vt)*f[15]*x[2]*x[11]; //p
}

void update_propensity_bound(double abound[], double f[], array<int,13> x, double VB, double Vbound, double tbound, double mu, int pstar){
    if (mu>=f[0]){
        cout<<"mu>=f[0]"<<endl;
    }
    double delay=(1/(1+exp(-f[16]*tbound)));
    
    double p0=(double(pstar)/VB)/sqrt(f[0]/mu-1);
    int allP=x[0]+x[9]+x[10];

    abound[0]=f[0]*pow(allP,1)/(1+pow((allP/Vbound)/p0,2)); //a
    abound[1]=f[1]*allP; //b
    abound[2]=f[2]*x[1]; //c
    abound[3]=delay*f[3]; //d
    abound[4]=f[4]*x[4]; //e
    abound[5]=f[5]*x[2]; //f
    abound[6]=f[5]*x[3]; //ff
    abound[7]=f[6]*x[5]; //g
    abound[8]=f[6]*x[6]; //gg
    abound[9]=(1/Vbound)*f[7]*x[4]*x[5]; //h
    abound[10]=(1/Vbound)*f[7]*x[4]*x[6]; //hh
    abound[11]=(1/Vbound)*f[8]*x[0]*x[7]; //i
    abound[12]=f[9]*x[9]; //j
    abound[13]=(1/Vbound)*f[10]*x[0]*x[8]; //k
    abound[14]=f[11]*x[10]; //l
    abound[15]=f[12]*x[9]; //m
    abound[16]=f[13]*x[10]; //n
    abound[17]=f[14]*x[11]; //o
    abound[18]=(1/Vbound)*f[15]*x[2]*x[11]; //p
}

double propensity_sum(double a[], int n){
    int i;
    double s=0.0;
    for(i=0; i<n; i++){
        s += a[i];}
    return(s);
}


/************************************************************
 *                                                          *
 *                     Variables                            *
 *                                                          *
 ************************************************************/
//model parameters
int numReactions, numSpecies;
double f[22], a[26], abound[26], a0, a0b;
double mu, Vt, Vbound, mLDiv, vLDiv, mDivR, vDivR;
double tgen, stdtgen;
array<int,13> xt;

//simulation parameters
int numTraj, numCells;
double dt, tFinal, L;
double tnextDiv;
int nextDiv;

int main(int argc, const char * argv[]) {
    //simulation parameters:
    dt=L=1; // simulation time step
    tFinal=2400; // final simulation time
    numTraj=100; // number of experiments
    numCells=100; // number of cells per experiment
    numReactions=19;
    numSpecies=12;
    int writeTraj=0; // when set to 1, outputs 1 example trajectory for each numTraj
    int rows=tFinal/dt;
    int pstar=5; // number of targets
    double VB=1.95; // average birth size of cell
    double * Survival=(double*)malloc(rows*sizeof(double));
    char cond[]="P"; // "DI" for direct interference, "P" for Priming. Change row ff of the stoichiometric matrix S accordingly.
    
    memset(Survival,0,rows*sizeof(double));
    
    //reaction network
    double CasF=1;
    double SIF=1;

    double f[17]={
    0.125,            // f[0] plasmid replication
    1.2,            // f[1] YFP production
    0,              // f[2] YFP degradation
    CasF*2.4,       // f[3] Cas production
    0,              // f[4] Cas degradation
    10,             // f[5] crRNA/crRNA* production
    0.014,          // f[6] crRNA/crRNA* degradation
    0.01,           // f[7] crRNA-Cas/crRNA*-Cas effector complex formation
    0.00001, 0.0001,// f[8], f[9] EP formation/dissociation
    0.001, 0.0001,  // f[10], f[11] EP* formation/dissociation
    1,              // f[12] plasmid degradation by EP
    1,              // f[13] plasmid degradation by EP*
    0.1,            // f[14] fragment degradation
    SIF*0.25,        // f[15] spacer integration
    0.025,};        // f[16] post-induction delay of protein production
    
    mDivR=0.5; // average division ratio
    vDivR=0.07*mDivR; // variance of division ratio
    normal_distribution<double> gaussian_length(mDivR,vDivR);  // division ratio distribution
    mLDiv=3.9; // average division length
    vLDiv=0.11*mLDiv; // variance of division length
    normal_distribution<double> DivLenDist(mLDiv,vLDiv); // division length distribution
    double tgen=70; // average generation time
    double vtgen=0.2; // variance of generation time
    double mu=log(2)/tgen; // growth rate
    lognormal_distribution<double> muDist(log(mu),vtgen); // growth rate distribution
    char label[50];
    snprintf(label,sizeof(label),"%s_P%d",cond,pstar); // File name identifier
    
    char filename1[100];
    char filename2[100];
    char filename3[100];
    char filename4[100];
    char filename5[100];
    snprintf(filename1, sizeof(filename1),"tau%s.txt",label); // Output file spacer acquisition (SA) time distribution
    snprintf(filename3, sizeof(filename3),"P0%s.txt",label); // Output file plasmid loss (PL) time distribution
    snprintf(filename4, sizeof(filename4),"parameters%s.txt",label); // Output file simulation parameters
    snprintf(filename5, sizeof(filename5),"survivalrate%s.txt",label); // Output file survival rate population
    ofstream tauOut(filename1);
    ofstream P0Out(filename3);
    ofstream parOut(filename4);
    ofstream SurvOut(filename5);
    
    parOut<<"dt = "<<dt<<endl;
    parOut<<"tFinal = "<<tFinal<<endl;
    parOut<<"numTraj = "<<numTraj<<endl;
    parOut<<"numCells = "<<numCells<<endl;
    parOut<<"tGen, Var(tGen) = "<<tgen<<", "<<vtgen<<endl;
    parOut<<"Div. length mean, var = "<<mLDiv<<", "<<vLDiv<<endl;
    parOut<<"Div. ratio mean, var = "<<mDivR<<", "<<vDivR<<endl;
    parOut<<"pstar = "<<pstar<<endl;
    parOut<<"burst Cas = "<<burstCas<<endl;
    parOut<<"burst F = "<<burstF<<endl;
    parOut<<"f = ";
    for (int i=0; i<(sizeof(f)/sizeof(*f)); i++) {
        parOut<<f[i]<<" ";
    }
    parOut<<endl;

    //First division algorithm:
    for (int k=0; k<numTraj; k++) { // ENSEMBLE
        snprintf(filename2, sizeof(filename2),"forward_traj%s_%d.txt",label,k); // Output file with molecule numbers recorded at regular time intervals of example lineage
        ofstream trajOut(filename2);
        printf("m = %d \t k = %d\n",m,k);
        vector<Cell> vectorOfCells;
        vectorOfCells.reserve(1000);
        int counter=0;
        int iTraj=1;

        //1. Initialisation of mother cell
        for (int n=0; n<numCells; n++) {
            double VD=DivLenDist(gen_n);
            double DivR=gaussian_length(gen_n);
            double VB=DivR*VD;
            double mu_c=muDist(gen_n);
            double tGen=(1/mu_c)*log(VD/VB);
            double tB=0;
            double ttDiv=0;
            while (ttDiv<=dt){// first div needs to be after dt
                tB=dis(gen_u);
                tB=(log(2.0)/mu_c)*tB-log(2.0)/mu_c;
                ttDiv=tB+tGen;
            }
            double Vt0=VB*exp(-mu_c*tB);
            int P_0=floor(pstar*Vt0);
            int Y_0=1000;
            //                     x0  x1  x2  x3  x4  x5  x6  x7  x8  x9 x10 x11
            array<int,13> x0P  = {P_0,Y_0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0}; // P
            array<int,13> x0DI = {P_0,Y_0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0}; // DI
            array<int,13> x0;
            if (strcmp(cond,"DI")==0) {
                x0 = x0DI;
            } else {
                x0 = x0P;
            }
            counter+=1;
            vector<int> history0;
            history0.push_back(counter);
            Cell newcell(history0,x0);
            newcell.tBirth=tB;
            newcell.tDiv=ttDiv;
            newcell.VBirth=VB;
            newcell.tgen_cell=tGen;
            newcell.tgen_cell_sum=tGen;
            newcell.mu_cell=mu_c;
            vectorOfCells.push_back(newcell);
        }

        // Determine next dividing cell:
        int inextDiv=0;
        tnextDiv=vectorOfCells[0].tDiv;
        for (int i=0; i<vectorOfCells.size(); i++) {
            double tDivi=vectorOfCells[i].tDiv;
            if (tDivi<tnextDiv) {
                inextDiv=i;
                tnextDiv=tDivi;
            }
        }
        for (int step=0; step<=rows; step++) { // TIME
            double t = step * dt;
            int numcleared=0;
            for (int i=0; i<vectorOfCells.size(); i++) {
                numcleared+=vectorOfCells[i].cleared;
            }

            Survival[step]+=numcleared;
            // write to file
            if (writeTraj) {
                for (int i=0; i<vectorOfCells.size(); i++) { // LOOP THROUGH CELLS
                    if (vectorOfCells[i].history.back()==iTraj) {
                        xt=vectorOfCells[i].x;
                        Vt=vectorOfCells[i].VBirth*exp(vectorOfCells[i].mu_cell*(t-vectorOfCells[i].tBirth));
                        trajOut<<iTraj<<"\t"<<t<<"\t"<<Vt<<"\t";
                        for (int j=0; j<numSpecies; j++) {
                            trajOut<<xt[j]<<"\t";
                        }
                        trajOut<<endl;
                    }
                }
            }

            double u=t;
            double tOut=t+dt;
            while (u<tOut) {
                // 2. Biochemical reactions
                for (int i=0; i<vectorOfCells.size(); i++) { // LOOP THROUGH CELLS
                    double s=u;
                    while (true) {
                        //1) Evaluate propensities and bound on propensities
                        xt=vectorOfCells[i].x;
                        Vt=vectorOfCells[i].VBirth*exp(vectorOfCells[i].mu_cell*(s-vectorOfCells[i].tBirth));
                        Vbound=vectorOfCells[i].VBirth*exp(vectorOfCells[i].mu_cell*(tOut-vectorOfCells[i].tBirth));
                        update_propensity_bound(abound, f, xt, VB, Vbound, tOut, vectorOfCells[i].mu_cell, pstar);
                        a0b=propensity_sum(abound, numReactions);
                        //2) Generate tau
                        double urnd1=dis(gen_u);
                        double tau=log(1/urnd1)/a0b;
                        //3) Advance time
                        if (s+tau>min(tOut,tnextDiv)) {
                            break;
                        }
                        if (tau>L) {
                            s+=L;
                            continue;
                        }
                        //4) Advance time
                        s+=tau;
                        //5) Choose reaction
                        Vt=vectorOfCells[i].VBirth*exp(vectorOfCells[i].mu_cell*(s-vectorOfCells[i].tBirth));
                        update_propensities(a, f, xt, VB, Vt, s, vectorOfCells[i].mu_cell, pstar);
                        a0=propensity_sum(a, numReactions);
                        double urnd2=dis(gen_u);
                        double r = a0b*urnd2;
                        if (a0>=r){
                            // Select reaction
                            double psum=a[0];
                            int reaction=0;
                            while (psum<r){
                                reaction+=1;
                                psum+=a[reaction];
                            }
                            // Update state
                            vectorOfCells[i].update_x(reaction);
                            if (reaction==18&&xt[3]==0){ // 1st spacer acquisition event
                                tauOut<<s<<"\t"<<double(vectorOfCells[i].tgen_cell_sum/vectorOfCells[i].history.size())<<"\t"<<vectorOfCells[i].history.size()<<"\t";
                                vectorOfCells[i].tau_cell=s;
                                vectorOfCells[i].taugen_cell=vectorOfCells[i].history.size();
                                for (int j = 0; j < vectorOfCells[i].history.size(); j++){
                                    tauOut << vectorOfCells[i].history[j] << " ";
                                }
                                tauOut<<endl;
                                

                            }
                            xt=vectorOfCells[i].x;
                            if ((reaction==15||reaction==16) &&xt[0]+xt[9]+xt[10]==0) { // all plasmids cleared
                                vectorOfCells[i].cleared=1;
                                P0Out<<s<<"\t"<<double(vectorOfCells[i].tgen_cell_sum/vectorOfCells[i].history.size())<<"\t"<<vectorOfCells[i].history.size()<<"\t"<<vectorOfCells[i].tau_cell<<"\t"<<vectorOfCells[i].taugen_cell<<endl;
                            }
                        }
                    } // WHILE TRUE end
                } // LOOP THROUGH CELLS end
                u=min(tnextDiv,tOut);
                if (u>=tnextDiv) {
                    // 3. Cell division
                    // initialise daughter cells
                    vector<int> history1=vectorOfCells[inextDiv].history;
                    counter+=1;
                    history1.push_back(counter);
                    vector<int> history2=vectorOfCells[inextDiv].history;
                    counter+=1;
                    history2.push_back(counter);
                    array<int,13> x1, x2;
                    Vt=vectorOfCells[inextDiv].VBirth*exp(vectorOfCells[inextDiv].mu_cell*(vectorOfCells[inextDiv].tDiv-vectorOfCells[inextDiv].tBirth));
                    double V1Birth=gaussian_length(gen_n)*Vt;
                    double V2Birth=Vt-V1Birth;
                    double lengthRatio=V1Birth/Vt;

                    // set division time of dividing cell to infinity
                    vectorOfCells[inextDiv].tDiv=tFinal;
                    xt=vectorOfCells[inextDiv].x;
                    binomial_distribution<int> bin1(xt[1],lengthRatio);
                    binomial_distribution<int> bin4(xt[4],lengthRatio);
                    binomial_distribution<int> bin5(xt[5],lengthRatio);
                    binomial_distribution<int> bin6(xt[6],lengthRatio);
                    binomial_distribution<int> bin7(xt[7],lengthRatio);
                    binomial_distribution<int> bin8(xt[8],lengthRatio);
                    binomial_distribution<int> bin11(xt[11],lengthRatio);

                    x1[1]=bin1(gen_b);
                    x1[2]=xt[2];
                    x1[3]=xt[3];
                    x1[4]=bin4(gen_b);
                    x1[5]=bin5(gen_b);
                    x1[6]=bin6(gen_b);
                    x1[7]=bin7(gen_b);
                    x1[8]=bin8(gen_b);
                    x1[11]=bin11(gen_b);
                    int source[3]={xt[0],xt[9],xt[10]};
                    int dest[3]={0, 0, 0};
                    int c=3; // number of plasmid-containing configurations
                    int n;
                    double toggle=dis(gen_u);
                    if (toggle<=0.5) {
                        n=floor((xt[0]+xt[9]+xt[10])*lengthRatio);
                    } else {
                        n=ceil((xt[0]+xt[9]+xt[10])*lengthRatio);
                    }
                    sto.MultiHypergeometric(dest,source,n,c);
                    x1[0]=dest[0];
                    x1[9]=dest[1];
                    x1[10]=dest[2];

                    x2[0]=xt[0]-x1[0];
                    x2[1]=xt[1]-x1[1];
                    x2[2]=xt[2];
                    x2[3]=xt[3];
                    x2[4]=xt[4]-x1[4];
                    x2[5]=xt[5]-x1[5];
                    x2[6]=xt[6]-x1[6];
                    x2[7]=xt[7]-x1[7];
                    x2[8]=xt[8]-x1[8];
                    x2[9]=xt[9]-x1[9];
                    x2[10]=xt[10]-x1[10];
                    x2[11]=xt[11]-x1[11];
                    Cell D1(history1,x1);
                    D1.tBirth=tnextDiv;
                    double V1Div=DivLenDist(gen_n);
                    while (V1Div<V1Birth) {
                        V1Div=DivLenDist(gen_n);
                    }
                    double muD1=muDist(gen_n);
                    double tGenD1=(1/muD1)*log(V1Div/V1Birth);
                    D1.tDiv=D1.tBirth+tGenD1;
                    D1.VBirth=V1Birth;
                    D1.tau_cell=vectorOfCells[inextDiv].tau_cell;
                    D1.taugen_cell=vectorOfCells[inextDiv].taugen_cell;
                    D1.tgen_cell=tGenD1;
                    D1.tgen_cell_sum=vectorOfCells[inextDiv].tgen_cell_sum+tGenD1;
                    D1.mu_cell=muD1;
                    if (x1[0]+x1[9]+x1[10]==0) {
                        D1.cleared=1;
                    } else {
                        D1.cleared=0;
                    }
                    vectorOfCells.push_back(D1);
                    Cell D2(history2,x2);
                    D2.tBirth=tnextDiv;
                    double V2Div=DivLenDist(gen_n);
                    while (V2Div<V2Birth) {
                        V2Div=DivLenDist(gen_n);
                    }
                    double muD2=muDist(gen_n);
                    double tGenD2=(1/muD2)*log(V2Div/V2Birth);
                    D2.tDiv=D2.tBirth+tGenD2;
                    D2.VBirth=V2Birth;
                    D2.tau_cell=vectorOfCells[inextDiv].tau_cell;
                    D2.taugen_cell=vectorOfCells[inextDiv].taugen_cell;
                    D2.tgen_cell=tGenD2;
                    D2.tgen_cell_sum=vectorOfCells[inextDiv].tgen_cell_sum+tGenD2;
                    D2.mu_cell=muD2;
                    if (x2[0]+x2[9]+x2[10]==0) {
                        D2.cleared=1;
                    } else {
                        D2.cleared=0;
                    }
                    vectorOfCells.push_back(D2);
                    if (V1Div<=V1Birth) {
                        cout<<"VB: "<<V1Birth<<"   VD: "<<V1Div<<endl;
                    }
                    if (V2Div<=V2Birth) {
                        cout<<"VB: "<<V2Birth<<"   VD: "<<V2Div<<endl;
                    }
                    if (xt[0]+xt[9]+xt[10]>0&&(x1[0]+x1[9]+x1[10]==0||x2[0]+x2[9]+x2[10]==0)) {
                        P0Out<<tnextDiv<<"\t"<<double(vectorOfCells[inextDiv].tgen_cell_sum/vectorOfCells[inextDiv].history.size())<<"\t"<<vectorOfCells[inextDiv].history.size()<<"\t"<<vectorOfCells[inextDiv].tau_cell<<"\t"<<vectorOfCells[inextDiv].taugen_cell<<endl;
                    }
                    if (vectorOfCells[inextDiv].history.back()==iTraj) {
                        iTraj=counter;
                    }
                    // remove dividing cell from vector
                    vectorOfCells.erase(vectorOfCells.begin() + inextDiv);

                    if (vectorOfCells.size()>numCells) {
                        // remove random cell from vector
                        uniform_int_distribution<int> choose2((int)0,(int)vectorOfCells.size()-(int)1); // number distribution
                        int elem=1000;
                        while(elem==1000||vectorOfCells[elem].history.back()==iTraj){
                            elem=choose2(engine);
                        }
                        
                        if (k==0) {
                            for (int j = 0; j < vectorOfCells[elem].history.size(); j++){
                                LinOut << vectorOfCells[elem].history[j] << " ";
                            }
                            LinOut<<endl;
                        }
                        vectorOfCells.erase(vectorOfCells.begin() + elem);
                    }
                    // Determine next dividing cell
                    tnextDiv=vectorOfCells[0].tDiv;
                    inextDiv=0;
                    for (int i=0; i<vectorOfCells.size(); i++) {
                        double tDivi=vectorOfCells[i].tDiv;
                        if (tDivi<tnextDiv) {
                            inextDiv=i;
                            tnextDiv=tDivi;
                        }
                    }
                    continue;
                }
                else if (u>=tOut){
                    break;
                }
                double tnextDiv=vectorOfCells[0].tDiv;
                int nextDiv=0;
                for (int i=0; i<vectorOfCells.size(); i++) {
                    double tDivi=vectorOfCells[i].tDiv;
                    if (tDivi<tnextDiv) {
                        nextDiv=i;
                        tnextDiv=tDivi;
                    }
                }
            } // WHILE U<TOUT end
        }// TIME end
        if (!writeTraj) {
            remove(filename2);
        }
    } // ENSEMBLE end
    for (int step=0; step<=rows; step++) {
        SurvOut<<Survival[step]/(numCells*numTraj)<<endl;
    }
    return 0;
} // MAIN end
