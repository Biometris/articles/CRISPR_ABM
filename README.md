CRISPR_ABM
================================

Author:		Emma Keizer <emma [dot] keizer1 [at] gmail [dot] com>
Date:		9 July 2021
Requirements:	C++ >= C++11

To compile script: 
g++ -std=c++11 SCRIPT_NAME.cpp -o YOUR_OUTPUT_NAME

To run script:
 ./YOUR_OUTPUT_NAME

All contents of this folder refers to “Single Cell Variability of CRISPR-Cas Interference and Adaptation” by McKenzie et al. (Nature Microbiology). 

The enclosed script simulates data used to calculate the plasmid loss time distribution of the agent-based model of a bacterial population with CRISPR-Cas.
Stochastic trajectories are simulated using the Extrande method by Voliotis et al. (PLOS Computational Biology, 12(6): e1004923, 2016). For more information on the simulation procedure and calculation of the autocorrelation and cell-cell correlation, refer to Supplementary Methods of “Single Cell Variability of CRISPR-Cas Interference and Adaptation” by McKenzie et al. (Nature Microbiology). 

The script generates the following output files, where "label" identifies a simulation run by condition (DI or P) and target copy number:
- label_parameters.txt: Contains settings used in the simulation
- label_tau.txt: Contains 4 columns: time until spacer acquisition (SA), average generation time up until SA, generations until SA, list of cell numbers in lineage
- label_P0.txt: Contains 5 columns: time until target loss (TL), average generation time up until TL, generations until TL, time until SA, generations until SA
- label_survivalrate.txt: Contains the fraction of cells in the population that have cleared all targets over time
- label_forward_traj: Contains numbers of all molecular species recorded at regular time intervals of example lineage 


